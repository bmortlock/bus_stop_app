# Feedback on software artefact

## Submission
 
+ Submission requirements were all followed.
+ GitLab repo is tidy.
+ Commit history suggests you didn't commit regularly. Practice doing so in your next project. It will become second nature eventually!
+ Project README is present but could use a bit more detail. What else would someone need to know or do to run the app? (I was forced to investigate main.py to find out what you'd called your database and collection in the code.)

## Architecture

+ You've kept it simple but have adhered to the Flask conventions.

## Data component

+ The data is logically structured. If you intended to introduce new features you might wish to adapt/extend this model.
+ You refer to the stops as 'A' and 'B', but are these their actual names?

## Quality of implementation

+ As an initial implementation this achieves its aims and demonstrates the ability to apply the taught techniques. For a higher mark you would be expected to build on the basics taught in class, demonstrating a higher level of independence.

There were some minor problems, mostly in relation to the template. For exampe, the HTML in home.html doesn't validate...which you'd have found out if you'd attempted to validate it! For each stop you have a single `<ul>` around all the related `<h3>` and `<li>` elements. It would have been 'semantically' correct to have had multiple `<ul>`'s. Think about it...if you were writing this data in a word document, you would write this as sub headings with a separate bulleted list below each one wouldn't you? 

Also, this code:

		{% for stop in doc["stop"] %}      
     		<li>{{ stop }}</li>                
     	{% endfor %}

Doesn't require the for loop because (according to your data) there is no array of stops per document (just a single text value). On that basis, the list is unnecessary too...you don't need to list one of something! 

You also neglected to include some of the essential markup that any HTML document needs (such as head and body tags). In the demo app, this was included in a basic 'boiler plate' template which could then be extended by other templates.

If any of this doesn't make sense, please refer to the corrections I made in your code or see me for clarification. 

Nevertheless, this is a solid piece of work with potential for extension.

# Feedback on written evaluation

## Knowledge

+ Some discussion of alternative architectural solutions though with slightly patchy knowledge. Here it may have been wise to refer to some reliable sources of information.
+ Good use of diagrams to express ideas and evidence knowledge.
+ Implementation process has been described in an appropriate level of detail with evidence you understand the code you've written.

## Critical thinking

+ The evaluation includes some evidence of problem-solving, although a few problems with the template appear not to have been noticed (refer to my feedback on the practical part of the problem).

## Academic quality

+ Submission requirements have been followed.
+ Document is fairly well structured but I'd avoid comma separated topics in a subheading. Make them 2 separate sections.
+ No external sources have been referenced in your report which is somewhat concerning. Did you not carry out any independent research at all?
+ Slightly on the short side though I know you've put a lot of effort in. Have you thought about contacting the Learning Hub for extra support with written work?

Overall this was a worthy effort which shows you have been able to apply many of the taught techniques in relation to a real-world problem.

Knowledge is very good but to improve further, you need to build on the taught techniques and extend your knowledge through independent research. I would also like to see more evidence of critical analysis.

Another area you can improve is with the structuring of the essay and referencing. Have you thought about contacting the Learning Hub for extra support with essays? It's not bad by any means, but a few tweaks here and there could make a big difference to the overall academic quality.
