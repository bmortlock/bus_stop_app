from flask import Flask, render_template
from flask_pymongo import PyMongo

app = Flask(__name__)
app.config['MONGO_URI'] = "mongodb://localhost:27017/bus_stops"
mongo = PyMongo(app)


@app.route('/')
def home():
    bus_routes = mongo.db.routes.find({})
    return render_template("home.html", data=bus_routes)


if __name__ == '__main__':
  	app.run(debug=True)

